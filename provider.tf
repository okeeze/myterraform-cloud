# terraform {
#   cloud {
#     organization = "meta-prof"
#     workspaces {
#       name = "jjtech-workspace"
#     }
#   }
#   # required_version = ">= 1.3.8"
#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = "4.55.0"
#     }

#   }
# }

terraform {
  # cloud {
  #   organization = "jjtech-learn"
  #   workspaces {
  #     name = "jjtech-workspace1"
  #   }
  # }
  # required_version = ">= 1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }


  }
}




provider "aws" {
  region = "us-east-2"
  #profile = "default"
}

# terraform {
#   backend "s3" {
#     # Replace this with your bucket name!
#     bucket         = "backend-meta"
#     key            = "jjtech/terraform.tfstate"
#     region         = "us-east-2"

#     # Replace this with your DynamoDB table name!
#     dynamodb_table = "mybackend"
#   }
#  }